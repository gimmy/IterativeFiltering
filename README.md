# Iterative Filtering in Reputation Systems: an implementation.
 after de Kerchove and P. van Dooren (2010)

Slides (in Italian) are available in the `slides` directory. 
Original article [here](http://perso.uclouvain.be/paul.vandooren/publications/deKerchoveV10.pdf).

## Compile
from the directory `fortran95`:
- Run `make` to compile the program.

- Run `make clean` to remove temporary files and executable.



