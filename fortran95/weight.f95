SUBROUTINE weight(r, w, d, k, items, raters)
  ! Calcolate the vector w using the belief divergence 
  ! vector d and the parameter k, with affine function

  INTEGER items ! numero oggetti
  INTEGER raters ! numero raters
  INTEGER :: j

  REAL, DIMENSION (items) :: r
  REAL, DIMENSION (raters) :: w, d
  REAL :: k, maxd

  ! Prendiamo k tale che il minimo
  ! delle componenti di w sia 0
  maxd =  MAXVAL(d)
  ! WRITE(*, FMT='(A, F5.3, A)', ADVANCE='NO') 'inverto ', maxd ! debug

  ! IF ( maxd .EQ. 0 ) THEN
  !    d_a_zero = d_a_zero +1
  ! END IF

  k = 1/maxd
  ! IF (ABS(k) < 1.0E-7) THEN
  !    k = 0
  !    WRITE(*,*) ' -> Ho messo k a 0'            ! debug
  ! ELSE
  !    WRITE(*,*) 'ottenendo k:', k            ! debug
  ! END IF

  ! Calcolo w = 1 - kd
  DO j=1,raters
     w(j) = 1 - k * d(j)
     ! WRITE(*,*) 'w(j):', w(j)   ! debug
  END DO


END SUBROUTINE weight
