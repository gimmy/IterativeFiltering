SUBROUTINE stop(r, w, diff, X, m, items, raters)

  IMPLICIT NONE

  REAL :: diff
  INTEGER items                 ! numero di oggetti
  INTEGER raters                ! numero di votanti
  REAL, DIMENSION (items) :: r, old_r, m
  REAL, DIMENSION (raters) :: w
  REAL, DIMENSION (items,raters) :: X
  ! INTEGER, DIMENSION (items,raters) :: X !, A
  INTEGER :: i, a_zero=0

  old_r = r

  ! Calcolo il nuovo r
  r = MATMUL(X,w) / m     ! (componentwise division)

  ! WRITE(*,*) 'old_r=', old_r
  ! WRITE(*,*) 'r=', r            ! debug

  CALL norma(r - old_r, diff, items, raters, a_zero)
  ! diff = ABS( SUM(r-old_r) )
  diff = SQRT(diff)
  WRITE(*,*) 'diff=', diff      ! vediamo se fa qualcosa

END SUBROUTINE
