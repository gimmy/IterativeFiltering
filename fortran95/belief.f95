SUBROUTINE belief(r, w, d, X, A, items, raters)
  ! Calcolate the belief divergence vector d
  IMPLICIT NONE
  INTEGER items ! numero oggetti
  INTEGER raters ! numero raters
  INTEGER :: i, j, a_zero=0

  REAL, DIMENSION (items) :: r, new_r
  REAL, DIMENSION (raters) :: w, d
  INTEGER, DIMENSION (raters) :: n

  REAL, DIMENSION (items,raters) :: X
  INTEGER, DIMENSION (items,raters) :: A

  ! Calcolo la differenza tra x_i e new_r
  DO j=1,raters

     ! Pulisco r per j-esimo rater...
     new_r = A(:,j)*r

     ! ...ed il vettore n che conta gli oggetti votati
     n(j) = SUM(A(:,j))
     
     CALL norma( X(:,j) - new_r, d(j), items, raters, a_zero )
     ! IF (ABS( d(j) ) < 1.0E-7) THEN
     !    d(j) = 0
     !    a_zero = a_zero +1
     ! ELSE
     !    d(j) = d(j) / n(j)
     ! END IF
     
     d(j) = d(j) / n(j)

  END DO

  ! WRITE(*, '(A,I10,A)') 'messo d(j) a 0 ', a_zero,' volte'

  ! Riscalo d
  ! d = d/items

END SUBROUTINE belief
