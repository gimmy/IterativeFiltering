PROGRAM iterativefiltering
  IMPLICIT NONE
  ! Lunghezza dei vettore
  INTEGER items                 ! numero di oggetti
  INTEGER raters                ! numero di votanti
  INTEGER ratings               ! numero di voti

  ! Dichiaro il vettore dei voti r
  REAL, DIMENSION (:), ALLOCATABLE :: r, m
  ! Quello dei pesi w e della divergenza di opinione
  REAL, DIMENSION (:), ALLOCATABLE :: w, d

  ! Parametro k, file su cui leggere e scrivere, contatori
  REAL :: k, diff=1
  REAL, PARAMETER ::  precision = 1.0e-3
  INTEGER, PARAMETER :: dati = 11 !, outfile = 12
  INTEGER :: i, j, h, timestamp, steps=0, file

  ! Dichiariamo la matrice dei voti e la sua matrice di adiacenza
  REAL, DIMENSION (:,:), ALLOCATABLE :: X 
  INTEGER, DIMENSION (:,:), ALLOCATABLE :: A

  ! Inserisci la dimensione del sistema
  ! WRITE(*, FMT='(A)', ADVANCE='NO') 'Insert number of items: '
  ! READ(*,*) items
  ! WRITE(*, FMT='(A)', ADVANCE='NO') 'Insert number of raters: '
  ! READ(*,*) raters
  ! WRITE(*, FMT='(A)', ADVANCE='NO') 'Insert number of ratings: '
  ! READ(*,*) ratings
  WRITE(*, FMT='(A)', ADVANCE='NO') 'Insert file (1,2,3,4): ' 
  READ(*,*) file

  ! Apriamo i file
  SELECT CASE (file)
  CASE (1)
     OPEN(FILE='ratings1.dat', UNIT=dati)
     items = 4
     raters = 3
     ratings = 12

  CASE (2)
     OPEN(FILE='ratings2.dat', UNIT=dati)
     items = 1682
     raters = 943
     ratings = 100000

  CASE (3)
     OPEN(FILE='ratings3.dat', UNIT=dati)
     items = 3952
     raters = 6040
     ratings = 1000209

  CASE (4)
     OPEN(FILE='ratings4.dat', UNIT=dati)
     items = 10681
     raters = 71567
     ratings = 10000054
  END SELECT
  ! OPEN(FILE='out.txt',UNIT=outfile)

  ! Allochiamo matrici e vettori
  ALLOCATE(X(items,raters))
  ALLOCATE(A(items,raters))
  ALLOCATE(r(1:items), m(1:items))
  ALLOCATE(w(1:raters), d(1:raters))

  ! Inizializiammo
  r = 0                         ! i voti a 0
  w = 1                         ! pesi partono da 1
  d = 0                         ! la divergenza di opinione da 0
  X = 0
  A = 0

  ! Leggiamo i dati e li scriviamo in X
  DO h=1,ratings
     READ(dati, *) j, i, X(i,j), timestamp
     ! READ(dati, '(I5.1,A,I5.1,A,I5.1,A,I10.5)') j, i, X(i,j), timestamp
     IF( X(i,j) .NE. 0 ) THEN
        A(i,j) = 1              ! riempio A se X(i,j) è non nullo
     END IF
     ! WRITE(*, '(A,I2,A,I2,A,I2,A,I2,A,I2,A,I2)') 'X(',i, ',',j, ' )=', X(i,j), ' -> A(', i, ',', j, ' )=', A(i,j) !debug
  END DO

  ! d = MATMUL(X,w)               ! momentaneo debug
  ! DO i=1,items
  !    IF ( d(i) .EQ. 0 ) THEN
  !       WRITE(*,'(A,I5,A,I5,A, F10.5)') "all'indice", i, ': d(',i,') è', d(i)
  !       stop '"esco dal ciclo"'
  !    END IF
  ! END DO

  ! Calcolo r 
  r = MATMUL(X,w)
  m = MATMUL(A,w)

  ! Controllo che le componenti di m siano diverse da 0, 
  ! altrimenti le metto a 1
  DO i=1,items
     IF ( m(i) .EQ. 0 ) THEN
        m(i) = 1
     END IF
     ! IF ( isnan(r(i)) ) THEN
     !    WRITE(*,'(A,I5,A, F5.3)') 'r(',i,'):', r(i)
     !    stop '"r(i) is NaN"'
     ! END IF
  END DO

  r = r / m     ! (componentwise division)

  DO  WHILE (diff .GE. precision)
     CALL belief(r, w, d, X, A, items, raters) ! Calcolo d
     ! WRITE(*,*) 'd=', d            ! debug

     CALL weight(r, w, d, k, items, raters) ! Calcolo w
     ! WRITE(*,*) 'w=', w            ! debug

     CALL stop(r, w, diff, X, m, items, raters)
     steps = steps + 1
  END DO

  WRITE(*, FMT='(A,I2)', ADVANCE='NO') 'steps:', steps ! 'r=', r
  WRITE(*,*) ! 'precision:', precision

  ! Dealloco
  DEALLOCATE(X,A)
  DEALLOCATE(r,w,d)

  ! Chiudiamo i file
  CLOSE(dati)
  ! CLOSE(outfile)

END PROGRAM iterativefiltering
