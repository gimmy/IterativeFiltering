SUBROUTINE norma(a, b, items, raters, a_zero)
  ! Calcola la norma 2 al quadrato del vettore a
  ! e la restituisce nel vettore b

  IMPLICIT NONE
  INTEGER items ! numero oggetti
  INTEGER raters ! numero raters

  INTEGER :: i, n, a_zero
  REAL, DIMENSION (items) :: a, c
  REAL :: b

  ! Calcolo i quadrati delle componenti
  DO i=1,items
     ! IF ( ABS(a(i)) < 1.0E-7 ) THEN
     !    c(i) = 0
     !    a_zero = a_zero +1
     ! ELSE
     !    c(i) = a(i)**2
     ! END IF

     c(i) = a(i)**2

     IF ( isnan(c(i)) ) THEN
        WRITE(*,'(A,I5,A, F5.3)') 'a(',i,'):', a(i)
        stop '"c(i) is NaN"'
     END IF
  END DO

  b = SUM(c)

END SUBROUTINE norma
